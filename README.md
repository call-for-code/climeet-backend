
## Prerequisities
- JDK 12+

## Installation
1. install all depedencies inside lib folder
2. setup database configuration on pom.xml
3. run application using `mvn clean install && java -jar target/disman-api-app.jar`
