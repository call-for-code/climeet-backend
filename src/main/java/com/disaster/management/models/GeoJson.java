package com.disaster.management.models;

import java.util.List;

public class GeoJson {

  private String type;
  private UserProfile properties;
  private Geometry geometry;


  public String getType() {
    return type;
  }

  public GeoJson setType(String type) {
    this.type = type;
    return this;
  }

  public UserProfile getProperties() {
    return properties;
  }

  public GeoJson setProperties(UserProfile properties) {
    this.properties = properties;
    return this;
  }

  public Geometry getGeometry() {
    return geometry;
  }

  public GeoJson setGeometry(Geometry geometry) {
    this.geometry = geometry;
    return this;
  }

  public GeoJson() {

  }

  public static class Geometry {
    private String type;
    private List<Float> coordinates = null;

    public String getType() {
      return type;
    }

    public Geometry setType(String type) {
      this.type = type;
      return this;
    }

    public List<Float> getCoordinates() {
      return coordinates;
    }

    public Geometry setCoordinates(List<Float> coordinates) {
      this.coordinates = coordinates;
      return this;
    }

    @Override
    public String toString() {
      return "Geometry{" +
          "type='" + type + '\'' +
          ", coordinates=" + coordinates +
          '}';
    }
  }

  @Override
  public String toString() {
    return "GeoJson{" +
        "type='" + type + '\'' +
        ", properties=" + properties +
        ", geometry=" + geometry +
        '}';
  }
}
