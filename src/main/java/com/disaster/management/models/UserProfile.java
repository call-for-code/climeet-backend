package com.disaster.management.models;


import id.masoft.guipig.core.model.JsonData;

import java.util.Date;

public class UserProfile implements JsonData {

  private Integer id;
  private String username;
  private String name;
  private String email;
  private String phone;
  private String gender;
  private String avatarImageUrl;
  private Date birthdate;
  private String address;
  private String addressLatLon;
  private String deviceLatLon;
  private String bio;

  public Integer getId() {
    return id;
  }

  public UserProfile setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getUsername() {
    return username;
  }

  public UserProfile setUsername(String username) {
    this.username = username;
    return this;
  }

  public String getName() {
    return name;
  }

  public UserProfile setName(String name) {
    this.name = name;
    return this;
  }

  public String getEmail() {
    return email;
  }

  public UserProfile setEmail(String email) {
    this.email = email;
    return this;
  }

  public String getPhone() {
    return phone;
  }

  public UserProfile setPhone(String phone) {
    this.phone = phone;
    return this;
  }

  public String getGender() {
    return gender;
  }

  public UserProfile setGender(String gender) {
    this.gender = gender;
    return this;
  }

  public String getAvatarImageUrl() {
    return avatarImageUrl;
  }

  public UserProfile setAvatarImageUrl(String avatarImageUrl) {
    this.avatarImageUrl = avatarImageUrl;
    return this;
  }

  public Date getBirthdate() {
    return birthdate;
  }

  public UserProfile setBirthdate(Date birthdate) {
    this.birthdate = birthdate;
    return this;
  }

  public String getAddress() {
    return address;
  }

  public UserProfile setAddress(String address) {
    this.address = address;
    return this;
  }

  public String getAddressLatLon() {
    return addressLatLon;
  }

  public UserProfile setAddressLatLon(String addressLatLon) {
    this.addressLatLon = addressLatLon;
    return this;
  }

  public String getBio() {
    return bio;
  }

  public UserProfile setBio(String bio) {
    this.bio = bio;
    return this;
  }

  public String getDeviceLatLon() {
    return deviceLatLon;
  }

  public UserProfile setDeviceLatLon(String deviceLatLon) {
    this.deviceLatLon = deviceLatLon;
    return this;
  }

  @Override
  public String toString() {
    return "UserProfile{" +
        "id=" + id +
        ", username='" + username + '\'' +
        ", name='" + name + '\'' +
        ", email='" + email + '\'' +
        ", phone='" + phone + '\'' +
        ", gender='" + gender + '\'' +
        ", birthdate=" + birthdate +
        ", address='" + address + '\'' +
        ", addressLatLon='" + addressLatLon + '\'' +
        ", bio='" + bio + '\'' +
        '}';
  }
}
