package com.disaster.management.controllers;


import com.disaster.management.helpers.UserHelper;
import com.disaster.management.models.UserProfile;
import com.sun.net.httpserver.HttpExchange;
import id.masoft.guipig.core.annotations.Controller;
import id.masoft.guipig.core.annotations.Get;
import id.masoft.guipig.core.annotations.Intercept;
import id.masoft.guipig.core.annotations.Parameter;
import id.masoft.guipig.core.annotations.Post;
import id.masoft.guipig.core.database.Query;
import id.masoft.guipig.core.database.QueryOperator;
import id.masoft.guipig.core.database.Where;
import id.masoft.guipig.core.exceptions.ValidationException;
import id.masoft.guipig.core.model.BaseResponse;
import id.masoft.guipig.module.auth.core.AuthInterceptor;
import id.masoft.guipig.module.auth.core.AuthUtil;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class FriendController {

  @Post(url = "/friend/add")
  @Intercept(clazz = AuthInterceptor.class, params = {
      @Parameter(key = "ALLOWED_ROLES", value = "SUPERUSER,USER")
  })
  public BaseResponse addFriend(Map<String, String> param, HttpExchange e) throws SQLException, ValidationException {
    UserProfile friendToAdd = UserHelper.getUserProfileFromUserName(param.get("username"));
    UserProfile currentUser = UserHelper.getUserProfileFromUserName(AuthUtil.getCurrentUsername(e));

    if (currentUser.getId().equals(friendToAdd.getId())) {
      throw new ValidationException("You cannot add yourself");
    }

    if (!new Query.From("user_friends").select()
        .where(new QueryOperator.Or(
            new QueryOperator.And(new Where("id_user", currentUser.getId()), new Where("id_friend", friendToAdd.getId())),
            new QueryOperator.And(new Where("id_user", friendToAdd.getId()), new Where("id_friend", currentUser.getId()))
        ))
        .execute().isEmpty()) {
      throw new ValidationException("You already request / became friend.");
    }
    new Query.Insert("user_friends").columns("id_user", "id_friend", "is_confirmed").values(currentUser.getId(), friendToAdd.getId(), false).execute();

    return new BaseResponse("Success Add Friend!");
  }

  @Post(url = "/friend/confirm")
  @Intercept(clazz = AuthInterceptor.class, params = {
      @Parameter(key = "ALLOWED_ROLES", value = "SUPERUSER,USER")
  })
  public BaseResponse confirmFriend(Map<String, String> param, HttpExchange e) throws SQLException, ValidationException {
    UserProfile friendRequestToAdd = UserHelper.getUserProfileFromUserName(param.get("username"));
    UserProfile currentUser = UserHelper.getUserProfileFromUserName(AuthUtil.getCurrentUsername(e));

    if (currentUser.getId().equals(friendRequestToAdd.getId())) {
      throw new ValidationException("You cannot confirm yourself");
    }

    if (!new Query.From("user_friends").select()
        .where("id_user", "=", friendRequestToAdd.getId())
        .where("id_friend", "=", currentUser.getId())
        .where("is_confirmed", true)
        .execute().isEmpty()) {
      throw new ValidationException("You already became friend.");
    }

    if (!new Query.Table("user_friends")
        .update()
        .where("id_user", "=", friendRequestToAdd.getId())
        .where("id_friend", "=", currentUser.getId())
        .set("is_confirmed", true)
        .execute()) {
      throw new ValidationException("Failed to confirm");
    }

    return new BaseResponse("Success Confirm Friend!");
  }

  @Get(url = "/friend")
  @Intercept(clazz = AuthInterceptor.class, params = {
      @Parameter(key = "ALLOWED_ROLES", value = "SUPERUSER,USER")
  })
  public List<UserProfile> getFriend(Map<String, String> param, HttpExchange e) throws SQLException, ValidationException {
    UserProfile currentUser = UserHelper.getUserProfileFromUserName(AuthUtil.getCurrentUsername(e));

    List<Map<String, Object>> userFriends = new Query.Table("user_friends").select()
        .where(new QueryOperator.Or(new Where("id_user", currentUser.getId()), new Where("id_friend", currentUser.getId())))
        .where("is_confirmed", "=", true)
        .execute()
        .getResults();

    List<Integer> listOfFriendsIds = userFriends.stream().map(friend -> {
      if (friend.get("id_user").equals(currentUser.getId())) {
        return (Integer) friend.get("id_friend");
      }
      return (Integer) friend.get("id_user");
    }).collect(Collectors.toList());

    return UserHelper.getUserProfilesByUserIds(listOfFriendsIds);
  }

  @Get(url = "/friend/unconfirmed")
  @Intercept(clazz = AuthInterceptor.class, params = {
      @Parameter(key = "ALLOWED_ROLES", value = "SUPERUSER,USER")
  })
  public List<UserProfile> getUnconfirmedFriend(Map<String, String> param, HttpExchange e) throws SQLException, ValidationException {
    UserProfile currentUser = UserHelper.getUserProfileFromUserName(AuthUtil.getCurrentUsername(e));

    List<Map<String, Object>> userFriends = new Query.Table("user_friends").select()
        .where("id_friend", currentUser.getId())
        .where("is_confirmed", false)
        .execute()
        .getResults();

    List<Integer> listOfFriendsIds = userFriends.stream().map(friend -> (Integer) friend.get("id_user")).collect(Collectors.toList());

    return UserHelper.getUserProfilesByUserIds(listOfFriendsIds);
  }

  @Get(url = "/friend/requested")
  @Intercept(clazz = AuthInterceptor.class, params = {
      @Parameter(key = "ALLOWED_ROLES", value = "SUPERUSER,USER")
  })
  public List<UserProfile> getRequestedFriend(Map<String, String> param, HttpExchange e) throws SQLException, ValidationException {
    UserProfile currentUser = UserHelper.getUserProfileFromUserName(AuthUtil.getCurrentUsername(e));

    List<Map<String, Object>> userFriends = new Query.Table("user_friends").select()
        .where("id_user", currentUser.getId())
        .where("is_confirmed", false)
        .execute()
        .getResults();

    List<Integer> listOfFriendsIds = userFriends.stream().map(friend -> (Integer) friend.get("id_friend")).collect(Collectors.toList());

    return UserHelper.getUserProfilesByUserIds(listOfFriendsIds);
  }


}
