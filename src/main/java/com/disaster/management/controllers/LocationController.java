package com.disaster.management.controllers;

import com.disaster.management.helpers.LocationHelper;
import com.disaster.management.helpers.UserHelper;
import com.disaster.management.models.GeoJson;
import com.disaster.management.models.UserProfile;
import com.sun.net.httpserver.HttpExchange;
import id.masoft.guipig.core.annotations.Controller;
import id.masoft.guipig.core.annotations.Intercept;
import id.masoft.guipig.core.annotations.Parameter;
import id.masoft.guipig.core.annotations.Post;
import id.masoft.guipig.core.exceptions.ValidationException;
import id.masoft.guipig.module.auth.core.AuthInterceptor;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Controller
public class LocationController {

  @Post(url = "/location/user")
  @Intercept(clazz = AuthInterceptor.class, params = {
      @Parameter(key = "ALLOWED_ROLES", value = "SUPERUSER,GOV_STAFF")
  })
  public List<GeoJson> getUserLocations(Map<String, String> param, HttpExchange e) throws SQLException, ValidationException {
    List<UserProfile> userProfiles = UserHelper.getAllUser();
    return LocationHelper.convertToGeoJsons(userProfiles);
  }
}
