package com.disaster.management.controllers;


import com.disaster.management.helpers.UserHelper;
import com.disaster.management.models.UserProfile;
import com.sun.net.httpserver.HttpExchange;
import id.masoft.guipig.core.annotations.Controller;
import id.masoft.guipig.core.annotations.Get;
import id.masoft.guipig.core.annotations.Intercept;
import id.masoft.guipig.core.annotations.Parameter;
import id.masoft.guipig.core.database.Query;
import id.masoft.guipig.core.exceptions.ValidationException;
import id.masoft.guipig.module.auth.core.AuthInterceptor;
import id.masoft.guipig.module.auth.core.AuthUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class PandemicController {

  @Get(url = "/pandemic/infection-rate")
  @Intercept(clazz = AuthInterceptor.class, params = {
      @Parameter(key = "ALLOWED_ROLES", value = "SUPERUSER,USER")
  })
  public List<Map<String, Object>> getTimeline(Map<String, String> param, HttpExchange e) throws SQLException, ValidationException {
    UserProfile currentUser = UserHelper.getUserProfileFromUserName(AuthUtil.getCurrentUsername(e));
    List<UserProfile> friendList = new ArrayList<>(new FriendController().getFriend(param, e));
    friendList.add(currentUser);

    Map<Integer, UserProfile> listOfFriendId = friendList.stream().collect(Collectors.toMap(UserProfile::getId, userProfile -> userProfile));

    List<Map<String, Object>> results = new Query.From("user_timelines").select().where("id_user", "in", new ArrayList<>(listOfFriendId.keySet())).execute().getResults();

    return results.stream().peek(result -> result.put("user", listOfFriendId.get((Integer) result.get("id_user")))).collect(Collectors.toList());
  }

}
