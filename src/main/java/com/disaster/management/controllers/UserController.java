package com.disaster.management.controllers;


import com.disaster.management.helpers.UserHelper;
import com.disaster.management.models.UserProfile;
import id.masoft.guipig.core.annotations.Controller;
import id.masoft.guipig.core.annotations.Get;
import id.masoft.guipig.core.annotations.Post;
import id.masoft.guipig.core.database.Query;
import id.masoft.guipig.core.exceptions.ValidationException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Controller
public class UserController {

  @Get(url = "/user/profile/{username}")
  public UserProfile getUserProfile(Map<String, String> param) throws SQLException, ValidationException {
    return UserHelper.getUserProfileFromUserName(param.get("username"));
  }

  @Post(url = "/user/search")
  public List<UserProfile> searchUser(Map<String, String> param) throws SQLException {
    List<Map<String, Object>> users = new Query.From("users").select().where("username", "like", param.get("username") + "%").orderBy("username").execute().getResults();
    if (users.isEmpty()) {
      return Collections.emptyList();
    }

    return UserHelper.toUserProfiles(users);
  }


}
