package com.disaster.management.interceptor;

import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import id.masoft.guipig.core.database.QuerySchema;
import id.masoft.guipig.core.exceptions.InterceptorException;
import id.masoft.guipig.core.interceptor.Interceptor;
import id.masoft.guipig.core.model.BaseResponse;
import id.masoft.guipig.core.server.GuiPigContext;
import id.masoft.guipig.core.server.Response;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class RegisterInterceptor extends Interceptor {

  private static final String DATA_KEY_USER_REQUEST_JSON = "userRequestJson";

  @Override
  public void interceptBefore(HttpExchange httpExchange) throws InterceptorException {
    JsonObject requestJson = Response.parseRequestBodyToJson(httpExchange);


    JsonObject userRequestJson = requestJson.deepCopy();
    userRequestJson.remove("password");
    userRequestJson.remove("role_id");

    JsonObject authRequestJson = new JsonObject();
    authRequestJson.addProperty("username", requestJson.get("username").getAsString());
    authRequestJson.addProperty("password", requestJson.get("password").getAsString());
    authRequestJson.addProperty("role_id", 4);

    putData(DATA_KEY_USER_REQUEST_JSON, userRequestJson);
    putData("username", authRequestJson.get("username").getAsString());

    InputStream overridedInputStream = new ByteArrayInputStream(authRequestJson.toString().getBytes());
    httpExchange.setStreams(overridedInputStream, httpExchange.getResponseBody());

  }

  @Override
  public void interceptAfter(HttpExchange httpExchange, Object handlerOutput) throws InterceptorException {
    BaseResponse handlerResponse = (BaseResponse) handlerOutput;
    JsonObject userToInsert = (JsonObject) getData(DATA_KEY_USER_REQUEST_JSON);

    //List<Map<String, Object>> queryResult = new Query.Table ("auth_users").select().where("username", getData("username").toString()).execute().getResults();
    if (!handlerResponse.getStatus()) {
      throw new InterceptorException("Failed to insert user");
    }
    userToInsert.addProperty("username", getData("username").toString());
    if (!new QuerySchema(
        GuiPigContext.getSchema("users")
    ).insert(
        userToInsert
    )) {
      throw new InterceptorException("Failed to insert user");
    }


  }
}
