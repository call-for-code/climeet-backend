package com.disaster.management.interceptor;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import id.masoft.guipig.core.constants.HttpVariables;
import id.masoft.guipig.core.database.Query;
import id.masoft.guipig.core.exceptions.InterceptorException;
import id.masoft.guipig.core.interceptor.Interceptor;
import id.masoft.guipig.core.model.BaseResponse;
import id.masoft.guipig.core.server.GuiPigHandler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class TimelineCategoryInterceptor extends Interceptor {

  @Override
  public void interceptBefore(HttpExchange httpExchange) throws InterceptorException {

  }

  @Override
  public void interceptAfter(HttpExchange httpExchange, Object o) throws InterceptorException {

    Map<String, Object> result = new Gson().fromJson((String) o, Map.class);
    List<Map<String, Object>> timelineCategories = (List<Map<String, Object>>) result.get("data");
    try {

      String etCategories = timelineCategories.stream().map(et -> (String) et.get("categories")).collect(Collectors.joining(","));
      List<String> listEtCategories = timelineCategories.stream().map(et -> (String) et.get("categories")).collect(Collectors.toList());
      Map<String, Map<String, Object>> mapTimelineCategories = timelineCategories.stream().collect(Collectors.toMap(tc -> ((String) tc.get("categories")), tc -> tc, (tc1,tc2) -> tc1));


      List<Map<String, Object>> extractedTimelines = new Query.From("extracted_user_timelines").select().where("category_relevant_text", "in", Arrays.asList(etCategories.split(","))).execute().getResults();


      extractedTimelines.forEach(et -> {
        //String category = getCategory(listEtCategories, (String) et.get("category_relevant_text"));
        listEtCategories.stream().filter(lec -> lec.contains((String) et.get("category_relevant_text"))).forEach(category->{
          if (null != category) {
            mapTimelineCategories.get(category).putIfAbsent("timelines", new ArrayList<>());
            ((List) mapTimelineCategories.get(category).get("timelines")).add(et);
          }
        });

      });

      byte[] output = GuiPigHandler.resolveOutput(new BaseResponse(mapTimelineCategories.values()));
      if (output == null) {
        output = "".getBytes();
      }

      httpExchange.sendResponseHeaders(HttpVariables.RESPONSE_CODE_OK, output.length);
      httpExchange.getResponseBody().write(output);
      httpExchange.getResponseBody().close();
    } catch (IOException | SQLException e) {
      throw new InterceptorException(e.getMessage());
    }
    return;
  }

  private String getCategory(List<String> listEtCategories, String containedString) {
    return listEtCategories.stream().filter(lec -> lec.contains(containedString)).findFirst().orElse(null);
  }

}
