package com.disaster.management.interceptor;

import com.auth0.jwt.interfaces.Claim;
import com.disaster.management.helpers.UserHelper;
import com.disaster.management.models.UserProfile;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import id.masoft.guipig.core.exceptions.InterceptorException;
import id.masoft.guipig.core.interceptor.Interceptor;
import id.masoft.guipig.core.server.Response;
import id.masoft.guipig.module.auth.core.AuthUtil;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;

public class TimelineInterceptor extends Interceptor {

  @Override
  public void interceptBefore(HttpExchange httpExchange) throws InterceptorException {
    try {
      Map<String, Claim> claims = AuthUtil.getClaims(httpExchange);
      String role = claims.get("role").asString();
      if (role.equals("USER") || role.equals("SUPERUSER")) {        //todo remove superuser
        UserProfile currentUser = UserHelper.getUserProfileFromUserName(claims.get("username").asString());

        JsonObject requestJson = Response.parseRequestBodyToJson(httpExchange).deepCopy();

        requestJson.addProperty("id_user", currentUser.getId());
        requestJson.addProperty("username", currentUser.getUsername());

        InputStream overridedInputStream = new ByteArrayInputStream(requestJson.toString().getBytes());
        httpExchange.setStreams(overridedInputStream, httpExchange.getResponseBody());
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new InterceptorException(e.getMessage());
    }
  }

  @Override
  public void interceptAfter(HttpExchange httpExchange, Object o) throws InterceptorException {

  }
}
