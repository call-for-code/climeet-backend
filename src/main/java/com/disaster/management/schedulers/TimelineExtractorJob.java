package com.disaster.management.schedulers;

import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.cloud.sdk.core.service.exception.BadRequestException;
import com.ibm.watson.language_translator.v3.LanguageTranslator;
import com.ibm.watson.language_translator.v3.model.IdentifiedLanguages;
import com.ibm.watson.language_translator.v3.model.IdentifyOptions;
import com.ibm.watson.language_translator.v3.model.TranslateOptions;
import com.ibm.watson.language_translator.v3.model.TranslationResult;
import com.ibm.watson.natural_language_understanding.v1.NaturalLanguageUnderstanding;
import com.ibm.watson.natural_language_understanding.v1.model.AnalysisResults;
import com.ibm.watson.natural_language_understanding.v1.model.AnalyzeOptions;
import com.ibm.watson.natural_language_understanding.v1.model.CategoriesOptions;
import com.ibm.watson.natural_language_understanding.v1.model.EntitiesOptions;
import com.ibm.watson.natural_language_understanding.v1.model.Features;
import id.masoft.guipig.core.database.Query;
import id.masoft.guipig.core.database.QueryResult;
import id.masoft.guipig.scheduler.Job;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;


public class TimelineExtractorJob extends Job {


  public static TimelineExtractorJob create() {
    TimelineExtractorJob job = new TimelineExtractorJob();
    job.setDelay(60);
    job.setTask(job.run());
    return job;
  }

  private Runnable run() {
    return () -> {
      Map<String, Object> atomicUserTimeline = null;
      try {
        QueryResult lastUserTimelineResult = new Query.From("user_timelines").select().where("is_extracted", false).execute();
        if (lastUserTimelineResult.getLast() == null) {
          return;
        }
        System.out.println(lastUserTimelineResult.getLast());
        Map<String, Object> lastUserTimeline = lastUserTimelineResult.getLast();
        atomicUserTimeline = new HashMap<>(lastUserTimeline);

        String text = (String) lastUserTimeline.get("description");


        IamAuthenticator authenticator = new IamAuthenticator("X5hAUK5fVRdMM48lJQeoM94VFPWthU75jl16Up3JW3xn");
        NaturalLanguageUnderstanding naturalLanguageUnderstanding = new NaturalLanguageUnderstanding(
            "2019-07-12", authenticator);
        naturalLanguageUnderstanding.setServiceUrl("https://api.eu-gb.natural-language-understanding.watson.cloud.ibm.com/instances/a570b0fc-1a0d-4336-9727-16fb4e1994a7");

        EntitiesOptions entitiesOptions = new EntitiesOptions.Builder()
            .emotion(true)
            .sentiment(true)
            .limit(2)
            .build();

        CategoriesOptions categoriesOptions = new CategoriesOptions.Builder()
            .explanation(true)
            .build();

        Features features = new Features.Builder()
            .entities(entitiesOptions)
            .categories(categoriesOptions)
            .build();

        AnalyzeOptions parameters = new AnalyzeOptions.Builder()
            .text(text)
            .features(features)
            .build();
        Integer idUserTimeline = (Integer) lastUserTimeline.get("id_user_timeline");
        try {
          lastUserTimeline.remove("id_user_timeline");
          lastUserTimeline.remove("created_at");
          lastUserTimeline.remove("is_extracted");
          lastUserTimeline.remove("updated_at");

          AnalysisResults response = naturalLanguageUnderstanding
              .analyze(parameters)
              .execute()
              .getResult();
          lastUserTimeline.put("category_label", response.getCategories().get(0).getLabel());
          lastUserTimeline.put("category_relevant_text", response.getCategories().get(0).getExplanation().getRelevantText().get(0));

          new Query.Insert("extracted_user_timelines").fromMap(lastUserTimeline).execute();
          new Query.From("user_timelines").update().set("is_extracted", true).where("id_user_timeline", idUserTimeline).execute();
        } catch (BadRequestException bre) {
          if (bre.getLocalizedMessage().contains("unsupported text language")) {
            IamAuthenticator languageAuthenticator = new IamAuthenticator("XmMWT_JKGY7yF6OrD2p0GXTrI34YT7XgnVB1PwRTizZp");
            LanguageTranslator languageTranslator = new LanguageTranslator("2018-05-01", languageAuthenticator);
            languageTranslator.setServiceUrl("https://api.eu-gb.language-translator.watson.cloud.ibm.com/instances/97e19ba0-7b09-4704-b862-e49d7038c9a3");

            IdentifyOptions identifyOptions = new IdentifyOptions.Builder()
                .text(text)
                .build();

            IdentifiedLanguages languages = languageTranslator.identify(identifyOptions)
                .execute().getResult();

            TranslateOptions translateOptions = new TranslateOptions.Builder()
                .addText(text)
                .modelId(languages.getLanguages().get(0).getLanguage() + "-en")
                .build();

            TranslationResult result = languageTranslator.translate(translateOptions)
                .execute().getResult();

            AnalyzeOptions fallbackParameters = new AnalyzeOptions.Builder()
                .text(result.getTranslations().get(0).getTranslation())
                .features(features)
                .build();

            AnalysisResults response = naturalLanguageUnderstanding
                .analyze(fallbackParameters)
                .execute()
                .getResult();

            lastUserTimeline.put("category_label", response.getCategories().get(0).getLabel());
            lastUserTimeline.put("category_relevant_text", response.getCategories().get(0).getExplanation().getRelevantText().get(0).getText());
            new Query.Insert("extracted_user_timelines").fromMap(lastUserTimeline).execute();
            new Query.From("user_timelines").update().set("is_extracted", true).where("id_user_timeline", idUserTimeline).execute();
          } // todo handle hanging user timeline if fails
          return;
        }

      } catch (Exception e) {
        if (null != atomicUserTimeline) {
          try {
            new Query.From("user_timelines").update().set("is_extracted", true).where("id_user_timeline", (Integer)atomicUserTimeline.get("id_user_timeline")).execute();
            atomicUserTimeline.put("category_label", "-");
            atomicUserTimeline.put("category_relevant_text", "N/A");
            atomicUserTimeline.remove("id_user_timeline");
            atomicUserTimeline.remove("created_at");
            atomicUserTimeline.remove("is_extracted");
            atomicUserTimeline.remove("updated_at");
            new Query.Insert("extracted_user_timelines").fromMap(atomicUserTimeline).execute();
          } catch (SQLException ex) {
            ex.printStackTrace();
          }
        }
        e.printStackTrace();
        return;
      }

    };
  }
}
