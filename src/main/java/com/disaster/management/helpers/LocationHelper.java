package com.disaster.management.helpers;


import com.disaster.management.models.GeoJson;
import com.disaster.management.models.UserProfile;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class LocationHelper {


  public static List<GeoJson> convertToGeoJsons(List<UserProfile> userProfiles) {
    return userProfiles.stream().map(LocationHelper::convertToGeoJson).filter(Objects::nonNull).collect(Collectors.toList());
  }

  public static GeoJson convertToGeoJson(UserProfile userProfile) {
    if (null == userProfile.getDeviceLatLon()) {
      return null;
    }
    List<Float> coordinates = Arrays.stream(userProfile.getDeviceLatLon().split(",")).map(Float::valueOf).collect(Collectors.toList());
    return new GeoJson()
        .setGeometry(
            new GeoJson.Geometry()
                .setType("Point")
                .setCoordinates(coordinates)
        ).setType(
            "Feature"
        ).setProperties(
            userProfile
        );
  }
}
