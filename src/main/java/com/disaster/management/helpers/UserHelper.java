package com.disaster.management.helpers;


import com.disaster.management.models.UserProfile;
import id.masoft.guipig.core.database.Query;
import id.masoft.guipig.core.exceptions.ValidationException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UserHelper {

  public static final String DEFAULT_AVATAR_IMG_URL = "https://cloud-object-storage-hw-cos-standard-bvb.s3.jp-tok.cloud-object-storage.appdomain.cloud/blank-profile-picture-973460_640.png";

  public static UserProfile getUserProfileFromUserName(String username) throws ValidationException, SQLException {
    Map<String, Object> user = new Query.From("users").select().where("username", username).execute().getFirst();
    if (null == user) {
      throw new ValidationException("User not found!");
    }
    return toUserProfile(user);
  }


  public static List<UserProfile> getAllUser() throws SQLException {
    List<Map<String, Object>> users = new Query.From("users")
        .select()
        .execute()
        .getResults();

    if (users.isEmpty()) {
      return Collections.emptyList();
    }

    return UserHelper.toUserProfiles(users);
  }

  public static List<UserProfile> getUserProfilesByUserIds(List<Integer> listOfIds) throws ValidationException, SQLException {
    if (listOfIds.isEmpty()) {
      return Collections.emptyList();
    }

    List<Map<String, Object>> users = new Query.From("users")
        .select()
        .where("id_user", "in", listOfIds)
        .execute()
        .getResults();

    if (users.isEmpty()) {
      return Collections.emptyList();
    }

    return UserHelper.toUserProfiles(users);
  }


  public static UserProfile toUserProfile(Map<String, Object> user) {
    return new UserProfile()
        .setId((Integer) user.get("id_user"))
        .setName((String) user.get("name"))
        .setUsername((String) user.get("username"))
        .setAddress((String) user.get("address"))
        .setAddressLatLon((String) user.get("address_lat_lon"))
        .setDeviceLatLon((String) user.get("device_lat_lon"))
        .setEmail((String) user.get("email"))
        .setAvatarImageUrl(user.get("avatar_img_url") == null ? DEFAULT_AVATAR_IMG_URL : (String) user.get("avatar_img_url"))
        .setPhone((String) user.get("phone"))                                  //todo add more info
        ;
  }


  public static List<UserProfile> toUserProfiles(List<Map<String, Object>> users) {
    return users.stream()
        .map(UserHelper::toUserProfile)
        .collect(Collectors.toList());
  }
}
