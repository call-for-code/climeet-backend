package com.disaster.management;

import com.disaster.management.interceptor.RegisterInterceptor;
import com.disaster.management.schedulers.TimelineExtractorJob;
import id.masoft.guipig.core.scanner.ControllerScanner;
import id.masoft.guipig.core.scanner.SchemaScanner;
import id.masoft.guipig.core.server.GuiPigServer;
import id.masoft.guipig.migrator.Migrator;
import id.masoft.guipig.module.auth.AuthModule;
import id.masoft.guipig.scheduler.GuiPigScheduler;

import java.io.IOException;

public class MainApplication {

  public static void main(String[] args) {
    try {
      var schemaList = SchemaScanner.schemasFrom("schemas");
      Migrator.from(schemaList)
          .rollback()
          .seed()
          .run();
      new GuiPigScheduler()
          .registerJob(TimelineExtractorJob.create())
          .run();
      GuiPigServer.create(System.getenv("PORT") != null ? Integer.parseInt(
          System.getenv("PORT")
      ) : 8000)
          .registerContext(schemaList)
          .registerContext(new ControllerScanner().scan("com.disaster.management.controllers"))
          .addModule(
              AuthModule.create()
                  .withRegisterInterceptor(new RegisterInterceptor())
          )
          .listen();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
