package com.disaster.management.enums;

public enum TimelineContent {
  TEXT, PHOTO, VIDEO
}
